package keys

import (
	"encoding/base64"
	"errors"
)

var (
	ErrPemDecodeFailed = errors.New("error: failed to decode key from file")
)

type MasterKey interface {
	Sign([]byte) ([]byte, error)
	Save(string) error
}

func encodeSig(sig []byte) []byte {
	encoded := []byte{}
	base64.StdEncoding.Encode(encoded, sig)
	return encoded
}
