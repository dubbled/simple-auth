package keys

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/pem"
	"io"
	"io/ioutil"
	"os"
	"reflect"
)

type RsaKey struct {
	*rsa.PrivateKey
}

func NewRSA(bits int) (*RsaKey, error) {
	key, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		return nil, err
	}
	if err = key.Validate(); err != nil {
		return nil, err
	}
	return &RsaKey{key}, err
}

func (k *RsaKey) Sign(payload []byte) ([]byte, error) {
	hash := sha512.Sum512(payload)
	s, err := k.PrivateKey.Sign(rand.Reader, hash[:], crypto.SHA512)
	if err != nil {
		return nil, err
	}

	return encodeSig(s), nil
}

func readRsaFrom(r io.Reader) (*rsa.PrivateKey, error) {
	keyF, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	encoded, _ := pem.Decode(keyF)
	if encoded == nil {
		return nil, ErrPemDecodeFailed
	}

	return x509.ParsePKCS1PrivateKey(encoded.Bytes)
}

func LoadRSA(path string) (*RsaKey, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	key, err := readRsaFrom(f)
	if err != nil {
		return nil, err
	}
	key.Precompute()
	return &RsaKey{key}, nil
}

func (k *RsaKey) writeTo(w io.Writer) error {
	enc := &pem.Block{
		Bytes: x509.MarshalPKCS1PrivateKey(k.PrivateKey),
	}
	return pem.Encode(w, enc)
}

func (k *RsaKey) ToFile(path string) error {
	f, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	if err := f.Truncate(0); err != nil {
		return err
	}

	return k.writeTo(f)
}

func (k1 *RsaKey) Cmp(k2 *RsaKey) bool {
	// if k1.D.Cmp(k2.D) != 0 ||
	// 	k1.N.Cmp(k2.N) != 0 ||
	// 	k1.E != k2.E ||
	// 	len(k1.Primes) != len(k2.Primes) {
	// 	return false
	// }

	// for i := range k1.Primes {
	// 	if k1.Primes[i].Cmp(k2.Primes[i]) != 0 {
	// 		return false
	// 	}
	// }
	// return true
	return reflect.DeepEqual(k1, k2)
}
