package keys

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha512"
	"crypto/x509"
	"encoding/pem"
	"io"
	"io/ioutil"
	"os"
	"reflect"
)

type EcdsaKey struct {
	*ecdsa.PrivateKey
}

func NewECDSA() (*EcdsaKey, error) {
	key, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	return &EcdsaKey{key}, err
}

func (k *EcdsaKey) Sign(payload []byte) ([]byte, error) {
	hash := sha512.Sum512(payload)
	s, err := k.PrivateKey.Sign(rand.Reader, hash[:], crypto.SHA512)
	if err != nil {
		return nil, err
	}

	return encodeSig(s), nil
}

func readEcdsaFrom(r io.Reader) (*ecdsa.PrivateKey, error) {
	keyF, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	encoded, _ := pem.Decode(keyF)
	if encoded == nil {
		return nil, ErrPemDecodeFailed
	}
	return x509.ParseECPrivateKey(encoded.Bytes)
}

func LoadECDSA(path string) (*EcdsaKey, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	key, err := readEcdsaFrom(f)
	if err != nil {
		return nil, err
	}
	return &EcdsaKey{key}, nil
}

func (k *EcdsaKey) writeTo(w io.Writer) error {
	b, err := x509.MarshalECPrivateKey(k.PrivateKey)
	if err != nil {
		return err
	}

	enc := &pem.Block{
		Bytes: b,
	}
	return pem.Encode(w, enc)
}

func (k *EcdsaKey) ToFile(path string) error {
	f, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	if err := f.Truncate(0); err != nil {
		return err
	}

	bytes, err := x509.MarshalECPrivateKey(k.PrivateKey)
	if err != nil {
		return err
	}

	enc := &pem.Block{
		Bytes: bytes,
	}

	return pem.Encode(f, enc)
}

func (k1 *EcdsaKey) Cmp(k2 *EcdsaKey) bool {
	// if k1.D.Cmp(k2.D) != 0 ||
	// 	k1.X.Cmp(k2.X) != 0 ||
	// 	k1.Y.Cmp(k2.Y) != 0 {
	// 	return false
	// }
	// return true
	return reflect.DeepEqual(k1, k2)
}
