package keys

import (
	"encoding/base64"
	"fmt"
	"log"
	"testing"
)

var key *RsaKey = nil

func TestMain(m *testing.M) {
	k, err := NewRSA(4096)
	if err != nil {
		log.Fatal(err)
	}
	key = k
}

func TestRsa(t *testing.T) {
	sig, err := key.Sign([]byte("sign this"))
	if err != nil {
		t.Error(err)
	}

	encoded := base64.StdEncoding.EncodeToString(sig)
	fmt.Println(encoded)
}
