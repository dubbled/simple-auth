package main

import (
	"encoding/json"
	"errl/simple-auth/keys"
	"errl/simple-auth/store"
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi"
)

type Authenticator struct {
	logger     *log.Logger
	privateKey keys.MasterKey
	store      store.Store
}

func NewAuthenticator(master keys.MasterKey, db store.Store) *Authenticator {
	return &Authenticator{
		privateKey: master,
		store:      db,
	}
}

func (a *Authenticator) HandleHttpRequests(port int) error {
	r := chi.NewRouter()
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("ok"))
	})
	r.Get("/record", func(w http.ResponseWriter, r *http.Request) {
		sig := []byte(r.Header.Get("auth_signature"))
		record, err := a.store.RecordFromSignature(sig)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		respText, err := json.Marshal(record)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}

		w.Write(respText)
	})

	srv := &http.Server{
		Addr:    fmt.Sprintf("0.0.0.0:%d", port),
		Handler: r,
	}

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		return err
	}
	return nil
}
