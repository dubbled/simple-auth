package store

type memStore map[string]Record

func NewMemStore() memStore {
	return memStore{}
}

func (m memStore) Records() ([]Record, error) {
	records := []Record{}
	for _, v := range m {
		records = append(records, v)
	}

	return records, nil
}

func (m memStore) RecordFromSignature(sig []byte) (*Record, error) {
	record := m[string(sig)]
	return &record, nil
}
