package store

import "time"

type Store interface {
	Records() ([]Record, error)
	RecordFromSignature([]byte) (*Record, error)
}

type Record struct {
	Sig          []byte    `db:"signature" json:"signature"`
	TimeInserted time.Time `db:"time_inserted" json:"time_inserted"`
}
