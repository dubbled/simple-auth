package store

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

type psql struct {
	*sqlx.DB
}

type psqlConfig struct {
	Host   string
	Port   int
	User   string
	DBName string
}

func NewPSQL(config *psqlConfig) (*psql, error) {
	db, err := sqlx.Open("postgres", fmt.Sprintf(`
	postgres://%s@%s/%s?sslmode=disable
	`, config.User, config.Host, config.DBName))

	if err := db.Ping(); err != nil {
		return nil, err
	}

	schema := `
	CREATE TABLE IF NOT EXISTS records (
		id				serial primary key,
		time_inserted	timestamp
	);
	`
	if _, err := db.Exec(schema); err != nil {
		return nil, err
	}

	return &psql{db}, err
}

func (db *psql) Records() ([]Record, error) {
	records := []Record{}
	err := db.Select(&records, `SELECT * FROM records`)
	return records, err
}

func (db *psql) RecordFromSignature(sig []byte) (*Record, error) {
	record := &Record{}
	err := db.Select(&record, `
	SELECT *
	FROM records
	WHERE signature = $1`, sig)
	return record, err
}
